import asyncio
import os
from aiogram import Dispatcher, Bot
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.middlewares.logging import LoggingMiddleware

from alertbot.settings import settings

loop = asyncio.get_event_loop()
bot = Bot(
    token=settings.TOKEN,
)
dp = Dispatcher(bot, storage=MemoryStorage())
dp.middleware.setup(LoggingMiddleware())


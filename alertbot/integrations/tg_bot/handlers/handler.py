from aiogram import types

from integrations.models import Message
from integrations.tg_bot.helper import dp, bot
from asgiref.sync import sync_to_async


async def BotHandler():
    me = await bot.get_me()
    print(me)


@dp.message_handler()
async def bot_action_select(message: types.Message):
    count = await save_chat_message(message)
    if count > 1:
        await bot.edit_message_text(message.text + f'\n count_message - {count}',
                                    message_id=await get_message_id(message.text),
                                    chat_id=message.chat.id)


@sync_to_async
def save_chat_message(message):
    Message.objects.create(message=message.text, message_id=message.message_id).save()
    count_message = Message.objects.filter(message=message.text).count()
    return count_message


@sync_to_async
def get_message_id(message_text):
    return Message.objects.filter(message=message_text).first()

from aiogram import Dispatcher
from aiogram.utils import executor
from integrations.tg_bot.handlers.handler import BotHandler
from integrations.tg_bot.helper import dp, loop


def BotInit():
    loop.run_until_complete(BotHandler())
    executor.start_polling(dp, skip_updates=True, on_shutdown=shutdown)


async def shutdown(dispatcher: Dispatcher):
    await dispatcher.storage.close()
    await dispatcher.storage.wait_closed()

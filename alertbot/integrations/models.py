from django.db import models


class Message(models.Model):
    message = models.TextField()
    message_id = models.IntegerField(unique=True, null=False, blank=False)

    def __str__(self):
        return f'{self.id}'

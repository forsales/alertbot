from django.core.management.base import BaseCommand
from integrations.tg_bot.main import BotInit


class Command(BaseCommand):
    help = ' OKR бот - помощник'

    def handle(self, *args, **options):
        BotInit()

